package com.featherlike.feather.generator.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.featherlike.framework.common.util.MapUtil;
import com.featherlike.framework.common.util.StringUtil;

public abstract class DBKeyInfo {
	List<String> keywordList = new ArrayList<String>();
	Map<String, String> typeMap = MapUtil.newHashMap();

	public String transformFieldName(String columnName) {
		String fieldName;
		if (keywordList.contains(columnName)) {
			fieldName = columnName + "_";
		} else {
			fieldName = columnName;
		}
		return StringUtil.toCamelhump(fieldName);
	}

	public String transformFieldType(String columnType) {
		String fieldType;
		if (typeMap.containsKey(columnType)) {
			fieldType = typeMap.get(columnType);
		} else {
			fieldType = "String";
		}
		return fieldType;
	}

	abstract void initKeyWords();
}
