/*****************************************************************
 *仿佛兮若轻云之蔽月，飘飘兮若流风之回雪
 *@filename Constant.java
 *@author WYY
 *@date 2013年11月24日
 *@copyright (c) 2013  wyyft@163.com All rights reserved.
 *****************************************************************/
package com.featherlike.feather.generator.config;

import java.io.File;

public interface Constant {
	String ENCODING_UTF8 = "UTF-8";
	String JXL = "JXL";
	String POI = "POI";
	String ENTITY = "entity";
	String DAO = "dao";
	String SERVICE = "service";
	String CONTROLLER = "controller";
	String WEB = "web";
	String TABLE = "table";
	String JAVA_SURFIX = ".java";
	String JSP_SURFIX = ".jsp";
	String SQL_SURFiX = ".sql";
	String FTL_SURFIX = ".ftl";
	String VIEW = "view";
	String VIEW_FORM = "Form";
	String View_LIST = "List";
	String KEY_TABLE = "table";
	String KEY_CLASSNAME = "className";
	String FREEMARKER = "freemarker";
	String VELOCITY = "velocity";
	String VM_SURFIX = ".vm";
	String FILE_SEPARATOR = File.separator;;
}
